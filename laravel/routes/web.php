<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@showSearchUsersByCountryPage');

Route::get('/get_users_by_country_name', 'UsersController@getUsersByCountryName')->name('get_users_by_country_name');
