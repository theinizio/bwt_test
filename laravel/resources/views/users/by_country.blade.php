<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>BWT test</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css"></link>
</head>
<body>
<div class="container">
    <div class="row m-auto">
        <form class="form form-inline p-5 m-auto" id="countryForm">
            <select name="countryName" class="w-75 form-control" id="countrySelect">
                @foreach($countries as $country)
                    <option @if($country->name === $selectedCountry) selected @endif value="{{$country->name}}">
                        {{$country->name}} ({{$country->companies_amount}})
                    </option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-info w-25">Show</button>
        </form>
    </div>

    @if($users->count())
        <div class="row">
            <table class="table table-hover table-active ">
                <tr>
                    <th colspan="3"><i>User</i></th>
                </tr>
                <tr class="bg-white">
                    <th></th>
                    <th>Company name</th>
                    <th>started working at</th>
                </tr>
                @foreach($users->sortBy('name') as $user)
                    <tr>
                        <td colspan="3"><i>{{ $user->name }} ({{ $user->email }})</i></td>
                    </tr>
                    @foreach($user->companies->sortByDesc(function ($company){return $company->pivot->created_at;}) as $company)
                        <tr class="bg-white">
                            <td></td>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->pivot->created_at->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endforeach
            </table>

        </div>
    @endif
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script>
    $('#countrySelect').change(() => {
        $('#countryForm').submit();
    });
</script>
</html>
