<?php

use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\User;
use Illuminate\Database\Seeder;

class CompanyUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::all()->map(function (User $user) {
            for ($i = 0; $i < random_int(0, 5); $i++) {
                $companyUser = new CompanyUser();
                $companyUser->user()->associate($user);
                $companyUser->company()->associate(Company::inRandomOrder()->first());
                $companyUser->created_at = now()->subDays(mt_rand(0, 10 * 365));
                $companyUser->save();
            }
        });
    }
}
