<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countryComponent = new \PragmaRX\Countries\Package\Countries();

        $countries = $countryComponent->all()->map(function ($country) {
            $newCountry = new Country();
            $newCountry->name = $country->name->first();
            $newCountry->iso_a2_code = $country->cca2;
            $newCountry->save();
        });

    }
}
