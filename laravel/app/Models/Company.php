<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Company extends Model
{
    /**
     * @return BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return BelongsToMany
     */
    public function users()
    {
        return $this
            ->belongsToMany(User::class, 'company_users')
            ->withPivot(['created_at']);
    }

    /**
     * @param string $countryName
     * @return Builder
     */
    public function getUsersByCountryName(string $countryName): Builder
    {
        return $this
            ->whereHas('country', function ($query) use ($countryName) {
                $query->where('name', '=', $countryName);
            })
            ->with('users');

    }

    /**
     * @return Collection
     */
    public static function getCountryNamesWithCompaniesAmount()
    {
        return self::query()
            ->join('countries', 'companies.country_id', '=', 'countries.id')
            ->groupBy('country_id')
            ->selectRaw('countries.name as name, count(countries.name) as companies_amount')
            ->orderByDesc('companies_amount')
            ->get();
    }
}
