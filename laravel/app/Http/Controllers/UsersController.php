<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersByCountryNameRequest;
use App\Models\Company;
use App\Models\Country;
use App\Models\User;

class UsersController extends Controller
{
    public function showSearchUsersByCountryPage(UsersByCountryNameRequest $request, User $user)
    {
        $countryNames = Company::getCountryNamesWithCompaniesAmount();
        $selectedCountryName = $request->get(
            'countryName',
            $countryNames->count() ? $countryNames->first()->name : ''
        );

        $usersWithCompanies = $user->usersByCountryName($selectedCountryName)->simplePaginate(20);

        return view('users.by_country', [
            'countries' => $countryNames,
            'selectedCountry' => $selectedCountryName,
            'users' => $usersWithCompanies,
        ]);
    }

    public function getUsersByCountryName(UsersByCountryNameRequest $request, User $user)
    {
        $countryName = $request->get('countryName');

        if (is_null($countryName) || strlen($countryName) === 0) {
            return response()->json([
                'status' => false,
                'message' => 'No country name was provided.'
            ]);
        }

        return response()->json([
            'status' => true,
            'companies' => $user->usersByCountryName($countryName)->simplePaginate(20)
        ]);
    }


}
