<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

### Installation:

1. Make a copy of the .env.dist for docker

    ```bash
    cp .env.dist .env
    ```
    ...and configure database connection:
    ```bash
    DOCKER_DATABASE=db_name
    DOCKER_USERNAME=db_user
    DOCKER_PASSWORD=db_password
    ```

2.  Make a copy of the .env.example for laravel
    ```bash
    cp laravel/.env.example laravel/.env
    ```
    ...and fill fields:
    ```bash
    DB_CONNECTION=mysql
    DB_HOST=db
    DB_PORT=3306
    DB_DATABASE=db_name
    DB_USERNAME=db_user
    DB_PASSWORD=db_password
    ```
    
    WARNING: DB_DATABASE, DB_USERNAME, DB_PASSWORD should be the same from docker env 
3. Create and start all containers 
    ```bash
    make up
    ```
4. Install dependencies via composer
    ```bash
    make vendor
    ```
    
5.  Gen the application key for the Laravel application
    ```bash
    make key
    ``` 

6. Install dependencies via npm
    ```bash
    make node_modules
    ```

    
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
